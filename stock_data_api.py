import yfinance as yf
from pprint import pprint
from serializer import Serializer
import pandas as pd
import datetime

class StockDataAPI(Serializer):
    
    def __init__(self, symbols_filename, stocks_filename):
        stock_filename = stocks_filename
        super(StockDataAPI, self).__init__(name=stock_filename)
        self.symbol_filename = symbols_filename
        self.stocks = {}

    # valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max (see https://pypi.org/project/yfinance/)
    def update_stocks(self, period='all'):
        symbols = self.parse_symbols()
        for symbol_data in symbols:
            data_frame = self.get_ticker_history(symbol_data["Symbol"], period=period)
            print(symbol_data["Symbol"], len(data_frame))
            if len(data_frame):
                self.stocks[symbol_data["Symbol"]] = data_frame
        return self.stocks

    def serialize(self):
        return self.stocks

    def deserialize(self, data):
        self.stocks = data
        return self.stocks

    def parse_symbols(self):
        with open(self.symbol_filename, "r") as file:
            output = []
            separator = "|"
            data = file.read().split("\n")
            header = data[0]
            names = header.split(separator)
            for line in data[1:]:
                parsed_line = {}
                values = line.split(separator)
                for name, value in zip(names, values):
                    if len(value) and "File Creation Time" not in value:
                        parsed_line[name] = value
                if len(parsed_line):
                    output.append(parsed_line)
        return output

    @staticmethod
    def get_ticker_history(symbol, period='max'):
        tickerData = yf.Ticker(symbol)
        return tickerData.history(period=period)


if __name__ == '__main__':
    api = StockDataAPI(symbols_filename="nasdaqlisted.txt", stocks_filename="stocks_data.pickle")
    api.update_stocks(period='1d')
    api.save()



import pickle
import os, sys

class Serializer:

    def __init__(self, name, path=""):
        self.name = name
        self.location = path

    def serialize(self):
        pass

    def deserialize(self, data):
        pass

    def save(self):
        full_path = os.path.join(self.location, self.name)
        if not full_path.endswith(".pickle"):
            full_path += ".pickle"
        with open(full_path, 'wb') as f:
            pickle.dump(self.serialize(), f)

    def load(self):
        full_path = os.path.join(self.location, self.name)
        if not full_path.endswith(".pickle"):
            full_path += ".pickle"
        with open(full_path, 'rb') as f:
            data = pickle.load(f)
        return self.deserialize(data)
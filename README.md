# Stocks analysis


Download the list of latest tickers symbols (nasdaqlisted.txt) at ftp.nasdaqtrader.com/SymbolDirectory/

Example to fetch and save stocks locally:

```
from stock_data_api import StockDataAPI

api = StockDataAPI(symbols_filename="path/to/nasdaqlisted.txt", stocks_filename="path/to/stocks_data.pickle")

# valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max (see https://pypi.org/project/yfinance/)
api.update_stocks(period='1d')

# save/serialize the stocks locally into "stock_data_current_datetime.pickle"
api.save()
```

To load saved stocks again: `api.load()`

The stocks are stored in `api.stocks` variable, in a dict with symbol as key and [yfinance](https://github.com/ranaroussi/yfinance) fetched dataframes as value. 

Example of format:
```
{'AAL':              Open   High   Low  Close    Volume  Dividends  Stock Splits
        Date                                                                    
        2020-04-17  11.87  11.94  11.5  11.57  59708718          0             0
    
}
```
